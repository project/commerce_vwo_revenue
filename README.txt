COMMERCE VWO REVENUE
--------------------

Commerce Visual Website Optimizer Revenue module adds the revenue tracking code on the order completion page.

Visual Website Optimizer is a split testing tool that allows you to easily setup A/B and Multivariate tests.

The revenue tracking code that is added on the order completion page is in this format:

<script>
var _vis_opt_revenue=100;
</script>

where 100 is the actual order total.


INSTALLATION INSTRUCTIONS
-------------------------

You only have to install the module and enable it for it to work.


NETSTUDIO
---------

This module is developed and sponsored by Netstudio.gr.
